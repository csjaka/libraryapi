var mysql = require('mysql');
var express = require('express');
var cors = require('cors');
var app = express();

app.use(express.json());
app.use(cors());

var connection = mysql.createConnection ({
        host: 'localhost',
        user: 'root',
        password: '',
        database : 'test_db'
});

app.get('/books', function(request, response){
    
    connection.query("SELECT  * FROM  book_info",  function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Successfully query!!!!!!!');
            console.log(rows.length);
            
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})
app.get('/staffs', function(request, response){
    
    connection.query("SELECT  * FROM  staff_info",  function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Successfully query!!!!!!!');
            console.log(rows.length);
            
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})
app.get('/borrower', function(request, response){
    
    connection.query("SELECT  * FROM  borrower_info",  function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Successfully query!!!!!!!');
            console.log(rows.length);
            
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})
app.get('/books/transactions', function(request, response){
    
    connection.query("SELECT trans_id,book_title,DATE_FORMAT(borrow_date, '%M %d %Y') as borrow_date,borrower_name,staff_name,IFNULL(DATE_FORMAT(return_date, '%M %d %Y'),'Unreturned') as return_date FROM transaction_info trans LEFT JOIN staff_info staff on trans.staff_id = staff.staff_id LEFT JOIN borrower_info borrower ON trans.borrower_id = borrower.borrower_id LEFT JOIN book_info book ON book.book_id = trans.book_id",  function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Successfully query!!!!!!!');
            console.log(rows.length);
            result = Object.values(JSON.parse(JSON.stringify(rows)))
            response.status(200).send(result);
        }
    });
})

app.post('/transaction/', function(request, response){

    console.log(request.body);
    staff_id = request.body.staff_id;
    borrower_id = request.body.borrower_id;
    book_id = request.body.book_id;
    borrow_date = request.body.borrow_date;
    return_date = request.body.return_date;
    
    connection.query(
        "insert into transaction_info (staff_id,borrower_id,book_id,borrow_date,return_date) values ('"+staff_id+"', '"+borrower_id+"', '"+book_id+"', '"+borrow_date+"','"+return_date+"')", 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Created Succesfully!!!!!!!');
            response.status(200).send();
        }
    });
})

app.delete('/transaction/:id', function(request, response){
    
    const { id } = request.params;
    connection.query(
        `delete from transaction_info where trans_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Deleted Succesfully!!!!!!!');
            response.status(200).send();
        }
    });
})



app.put('/transaction/:id', function(request, response){
    
    const { id } = request.params;
    return_date = request.body.return_date;

    connection.query(
        `update transaction_info set return_date = '${return_date}' WHERE trans_id = '${id}'`, 
            function(err, rows, fields)
    {      
        if(err){
            response.status(500).send();
        }else{
            console.log('Updated Succesfully!!!!!!!');
            response.status(200).send();
        }
    });
})

app.listen(4000);